//
//  Loading.swift
//  CashStash
//
//  Created by apple on 5/16/18.
//  Copyright © 2018 havells. All rights reserved.
//

import Foundation
import NVActivityIndicatorView
import UIKit

let activityData = ActivityData()

func showActivityIndicator(){
    NVActivityIndicatorView.DEFAULT_COLOR = UIColor.white
    NVActivityIndicatorView.DEFAULT_BLOCKER_MESSAGE = "loading.."
    NVActivityIndicatorView.DEFAULT_BLOCKER_BACKGROUND_COLOR = UIColor.black.withAlphaComponent(0.4)
    NVActivityIndicatorView.DEFAULT_TYPE = .lineSpinFadeLoader
    NVActivityIndicatorView.DEFAULT_BLOCKER_SIZE = CGSize(width: 25, height: 25)
    NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
}
func hideActivityIndicator(){
   NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
}
