//
//  AlertHelper.swift
//  XLobs
//
//  Created by apple on 9/25/18.
//  Copyright © 2018 apple. All rights reserved.
//

import Foundation

class AlertHelper{
    static func alertBox (Mymsg:String, view: UIViewController){
        let alert = UIAlertController(title: "", message: Mymsg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        view.present(alert, animated: true, completion: nil)
    }
}
