//
//  BuyOrderTableVwCell.swift
//  XLobs
//
//  Created by apple on 11/26/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit

class BuyOrderTableVwCell: UITableViewCell {

    @IBOutlet weak var buyFirstLbl: UILabel!
    @IBOutlet weak var buySecondLbl: UILabel!
    @IBOutlet weak var buyThirdLbl: UILabel!
    @IBOutlet weak var buyFourthLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
