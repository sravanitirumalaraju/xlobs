//
//  WalletTableViewCell.swift
//  XLobs
//
//  Created by apple on 9/19/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit

class WalletTableViewCell: UITableViewCell {

    @IBOutlet weak var coinImageVw: UIImageView!
    @IBOutlet weak var coinNameLbl: UILabel!
    @IBOutlet weak var coinsNumberLbl: UILabel!
    @IBOutlet weak var transacLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var coinButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        coinButton.layer.cornerRadius = 15
        coinButton.clipsToBounds = true
        
        coinButton.layer.shadowColor = UIColor(red: 22.0/255.0, green: 17.0/255.0, blue: 44.0/255.0, alpha: 0.05).cgColor
        coinButton.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        coinButton.layer.shadowOpacity = 25.0
        coinButton.layer.shadowRadius = 10.0
        coinButton.layer.masksToBounds = false
        
//        coinButton.layer.shadowColor = UIColor(white: 0.0, alpha: 0.25).cgColor
//        coinButton.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
//        coinButton.layer.shadowOpacity = 1.0
//        coinButton.layer.shadowRadius = 10
//        coinButton.layer.masksToBounds = false
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
