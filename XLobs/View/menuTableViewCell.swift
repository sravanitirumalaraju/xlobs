//
//  menuTableViewCell.swift
//  XLobs
//
//  Created by apple on 9/24/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit

class menuTableViewCell: UITableViewCell {

    //thirdview related
    @IBOutlet weak var singleTextVw: UIView!
    @IBOutlet weak var singleTitle: UILabel!
    //secondview related
    @IBOutlet weak var secVerTextVw: UIView!
    @IBOutlet weak var secVerMenuTitle: UILabel!
    @IBOutlet weak var secVerImage: UIImageView!
    //topview related
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var menuImage: UIImageView!
    @IBOutlet weak var menuTitle: UILabel!
    
    @IBOutlet weak var topLineLbl: UILabel!
    @IBOutlet weak var singleLineLbl: UILabel!
    @IBOutlet weak var secVerLineLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
