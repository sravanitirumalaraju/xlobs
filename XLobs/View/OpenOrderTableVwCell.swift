//
//  OpenOrderTableVwCell.swift
//  XLobs
//
//  Created by apple on 11/26/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit

class OpenOrderTableVwCell: UITableViewCell {

    @IBOutlet weak var openFirstLbl: UILabel!
    @IBOutlet weak var openSecondLbl: UILabel!
    @IBOutlet weak var openThirdLbl: UILabel!
    @IBOutlet weak var openFourthLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
