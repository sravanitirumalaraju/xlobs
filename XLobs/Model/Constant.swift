//
//  Constant.swift
//  XLobs
//
//  Created by apple on 9/25/18.
//  Copyright © 2018 apple. All rights reserved.
//

import Foundation

//var BaseURL = "https://xlob.herokuapp.com/"
//var BaseURL = "https://user.xlobs.in/"
var BaseURL = "https://api.xlobs.in/"
var signUpURL = BaseURL + "api/auth/user/signup"
var logInURL = BaseURL + "api/auth/user/verify"
var GetUserProfileURL = BaseURL + "api/data/fetch/user_details/0/0"
