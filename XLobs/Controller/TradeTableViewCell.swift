//
//  TradeTableViewCell.swift
//  XLobs
//
//  Created by apple on 11/26/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit

class TradeTableViewCell: UITableViewCell {

    @IBOutlet weak var exchangeBtn: UIButton!
    @IBOutlet weak var placeOrderBtn: UIButton!
    @IBOutlet weak var limitPriceTxtFld: UITextField!
    @IBOutlet weak var amountTxtFld: UITextField!
    @IBOutlet weak var segmentSelection: UISegmentedControl!
    @IBOutlet weak var limitBtn: UIButton!
    @IBOutlet weak var limitLineLbl: UILabel!
    @IBOutlet weak var marketLineLbl: UILabel!
    @IBOutlet weak var marketBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        exchangeBtn.layer.cornerRadius = 5
        exchangeBtn.clipsToBounds = true
        placeOrderBtn.layer.cornerRadius = 5
        placeOrderBtn.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
