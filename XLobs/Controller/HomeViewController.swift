//
//  HomeViewController.swift
//  XLobs
//
//  Created by apple on 9/20/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIScrollViewDelegate {

    @IBOutlet weak var topHeaderVw: UIView!
    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var bottomHeaderVw: UIView!
    
    var (tradeTableView,openTableView,buyTableView,sellTableView,recentTableView,pendingTableView) = (UITableView(),UITableView(),UITableView(),UITableView(),UITableView(),UITableView())
    var homeContentScrlVw = UIScrollView()
    var topScrollview = UIScrollView()
    var highlightLbl = UILabel()
    let (tradeBtn,openOrdersBtn,buyOrderBtn,sellOrdersBtn,recentTradeBtn,pendingOrdersBtn) = (UIButton(),UIButton(),UIButton(),UIButton(),UIButton(),UIButton())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if revealViewController() != nil {
            menuBtn.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.navigationController?.navigationBar.isHidden = true
        }
        highlightLbl.text = ""
        highlightLbl.backgroundColor = UIColor(red: 0.0/255.0, green: 122.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        topScrollview.addSubview(highlightLbl)
        topScrollVwContent()
        scrollVwcontent()
    }
    func topScrollVwContent(){
        
        if #available(iOS 11.0, *) {
            //other devices
            if (UIApplication.shared.keyWindow?.safeAreaInsets.top)! == 0{
                topScrollview.frame = CGRect(x: 0, y: topHeaderVw.frame.origin.y + topHeaderVw.frame.size.height + 10, width: self.view.frame.size.width, height: 60)
            }else{//iphone x
                topScrollview.frame = CGRect(x: 0, y: topHeaderVw.frame.origin.y + topHeaderVw.frame.size.height + (UIApplication.shared.keyWindow?.safeAreaInsets.top)!, width: self.view.frame.size.width, height: 60)
            }
        }else{
            topScrollview.frame = CGRect(x: 0, y: topHeaderVw.frame.origin.y + topHeaderVw.frame.size.height + 10, width: self.view.frame.size.width, height: 60)
        }
        topScrollview.delegate = self
        topScrollview.backgroundColor = UIColor.white
        topScrollview.showsHorizontalScrollIndicator = false
        self.view.addSubview(topScrollview)
        
        tradeBtn.frame = CGRect(x: 0, y: 0, width: 70, height: 56)
        tradeBtn.titleLabel?.font = UIFont(name: "Lato-Bold", size: 12)
        tradeBtn.setTitle("TRADE", for: .normal)
        tradeBtn.setTitleColor(UIColor.init(red: 42.0/255.0, green: 55.0/255.0, blue: 99.0/255.0, alpha: 1.0), for: .normal)
        tradeBtn.addTarget(self, action: #selector(tradeBtnClicked(_:)), for: .touchUpInside)
        topScrollview.addSubview(tradeBtn)
        
        highlightLbl.frame = CGRect(x: tradeBtn.center.x - 10, y: tradeBtn.frame.origin.y + tradeBtn.frame.size.height, width: 20, height: 4)
        
        openOrdersBtn.frame = CGRect(x: tradeBtn.frame.origin.x + tradeBtn.frame.size.width, y: 0, width: 100, height: 56)
        openOrdersBtn.titleLabel?.font = UIFont(name: "Lato-Bold", size: 12)
        openOrdersBtn.setTitle("OPEN ORDERS", for: .normal)
        openOrdersBtn.setTitleColor(UIColor.init(red: 42.0/255.0, green: 55.0/255.0, blue: 99.0/255.0, alpha: 1.0), for: .normal)
        openOrdersBtn.addTarget(self, action: #selector(openOrdersClicked(_:)), for: .touchUpInside)
        topScrollview.addSubview(openOrdersBtn)
        
        buyOrderBtn.frame = CGRect(x: openOrdersBtn.frame.origin.x + openOrdersBtn.frame.size.width, y: 0, width: 90, height: 56)
        buyOrderBtn.titleLabel?.font = UIFont(name: "Lato-Bold", size: 12)
        buyOrderBtn.setTitle("BUY ORDER", for: .normal)
        buyOrderBtn.setTitleColor(UIColor.init(red: 42.0/255.0, green: 55.0/255.0, blue: 99.0/255.0, alpha: 1.0), for: .normal)
        buyOrderBtn.addTarget(self, action: #selector(buyOrderClicked(_:)), for: .touchUpInside)
        topScrollview.addSubview(buyOrderBtn)
        
        sellOrdersBtn.frame = CGRect(x: buyOrderBtn.frame.origin.x + buyOrderBtn.frame.size.width, y: 0, width: 100, height: 56)
        sellOrdersBtn.titleLabel?.font = UIFont(name: "Lato-Bold", size: 12)
        sellOrdersBtn.setTitle("SELL ORDERS", for: .normal)
        sellOrdersBtn.setTitleColor(UIColor.init(red: 42.0/255.0, green: 55.0/255.0, blue: 99.0/255.0, alpha: 1.0), for: .normal)
        sellOrdersBtn.addTarget(self, action: #selector(sellOrdersClicked(_:)), for: .touchUpInside)
        topScrollview.addSubview(sellOrdersBtn)
        
        recentTradeBtn.frame = CGRect(x: sellOrdersBtn.frame.origin.x + sellOrdersBtn.frame.size.width, y: 0, width: 110, height: 56)
        recentTradeBtn.titleLabel?.font = UIFont(name: "Lato-Bold", size: 12)
        recentTradeBtn.setTitle("RECENT TRADE", for: .normal)
        recentTradeBtn.setTitleColor(UIColor.init(red: 42.0/255.0, green: 55.0/255.0, blue: 99.0/255.0, alpha: 1.0), for: .normal)
        recentTradeBtn.addTarget(self, action: #selector(recentTradeClicked(_:)), for: .touchUpInside)
        topScrollview.addSubview(recentTradeBtn)
        
        pendingOrdersBtn.frame = CGRect(x: recentTradeBtn.frame.origin.x + recentTradeBtn.frame.size.width, y: 0, width: 130, height: 56)
        pendingOrdersBtn.titleLabel?.font = UIFont(name: "Lato-Bold", size: 12)
        pendingOrdersBtn.setTitle("PENDING ORDERS", for: .normal)
        pendingOrdersBtn.setTitleColor(UIColor.init(red: 42.0/255.0, green: 55.0/255.0, blue: 99.0/255.0, alpha: 1.0), for: .normal)
        pendingOrdersBtn.addTarget(self, action: #selector(pendingOrdersClicked(_:)), for: .touchUpInside)
        topScrollview.addSubview(pendingOrdersBtn)
        
        topScrollview.contentSize = CGSize(width: 600, height: 60)
    }
    func scrollVwcontent(){
        if #available(iOS 11.0, *) {
//            //other devices
//            if (UIApplication.shared.keyWindow?.safeAreaInsets.top)! == 0{
//                let scrollviewHeight = (topScrollview.frame.origin.y + topScrollview.frame.size.height + bottomHeaderVw.frame.size.height)
//                homeContentScrlVw.frame = CGRect(x: 0, y: topScrollview.frame.origin.y + topScrollview.frame.size.height, width: self.view.frame.size.width, height: self.view.frame.size.height - scrollviewHeight)
//            }else{//iphone x
                let scrollviewHeight = topScrollview.frame.origin.y + topScrollview.frame.size.height + bottomHeaderVw.frame.size.height + (UIApplication.shared.keyWindow?.safeAreaInsets.bottom)! + (UIApplication.shared.keyWindow?.safeAreaInsets.top)! + 20
                homeContentScrlVw.frame = CGRect(x: 0, y: topScrollview.frame.origin.y + topScrollview.frame.size.height + 20, width: self.view.frame.size.width, height: self.view.frame.size.height - scrollviewHeight)
//            }
        }else{
            homeContentScrlVw.frame = CGRect(x: 0, y: topScrollview.frame.origin.y + topScrollview.frame.size.height, width: self.view.frame.size.width, height: self.view.frame.size.height - (topScrollview.frame.origin.y + topScrollview.frame.size.height + bottomHeaderVw.frame.size.height))
        }
        
        homeContentScrlVw.delegate = self
        homeContentScrlVw.isPagingEnabled = true
        self.view.addSubview(homeContentScrlVw)
        
        tradeContent()
        openOrderContent()
        buyOrderContent()
        sellOrdersContent()
        recentTradeContent()
    }
    func tradeContent(){
        tradeTableView.frame = CGRect(x: 0, y: 0, width: CGFloat(self.view.frame.size.width), height: CGFloat(self.homeContentScrlVw.frame.size.height))
        tradeTableView.separatorStyle = .none
        tradeTableView.register(UINib(nibName: "TradeTableViewCell", bundle: nil), forCellReuseIdentifier: "TradeTableViewCell")
        tradeTableView.tag = 0
        tradeTableView.bounces = false
        tradeTableView.delegate = self
        tradeTableView.dataSource = self
        tradeTableView.backgroundColor =  UIColor.white
        homeContentScrlVw.addSubview(tradeTableView)
        
    }
    func openOrderContent(){
        openTableView.frame = CGRect(x: (self.view.frame.size.width), y: 0, width: CGFloat(self.view.frame.size.width), height: CGFloat(self.homeContentScrlVw.frame.size.height))
        openTableView.separatorStyle = .none
        openTableView.register(UINib(nibName: "OpenOrderTableVwCell", bundle: nil), forCellReuseIdentifier: "OpenOrderTableVwCell")
        openTableView.tag = 1
        openTableView.bounces = false
        openTableView.delegate = self
        openTableView.dataSource = self
        openTableView.backgroundColor =  UIColor.white
        homeContentScrlVw.addSubview(openTableView)
    }
    func buyOrderContent(){
        buyTableView.frame = CGRect(x: (self.view.frame.size.width * 2), y: 0, width: CGFloat(self.view.frame.size.width), height: CGFloat(self.homeContentScrlVw.frame.size.height))
        buyTableView.separatorStyle = .none
        buyTableView.register(UINib(nibName: "BuyOrderTableVwCell", bundle: nil), forCellReuseIdentifier: "BuyOrderTableVwCell")
        buyTableView.tag = 2
        buyTableView.bounces = false
        buyTableView.delegate = self
        buyTableView.dataSource = self
        buyTableView.backgroundColor =  UIColor.white
        homeContentScrlVw.addSubview(buyTableView)
    }
    func sellOrdersContent(){
        sellTableView.frame = CGRect(x: (self.view.frame.size.width * 3), y: 0, width: CGFloat(self.view.frame.size.width), height: CGFloat(self.homeContentScrlVw.frame.size.height))
        sellTableView.separatorStyle = .none
        sellTableView.register(UINib(nibName: "BuyOrderTableVwCell", bundle: nil), forCellReuseIdentifier: "BuyOrderTableVwCell")
        sellTableView.tag = 3
        sellTableView.bounces = false
        sellTableView.delegate = self
        sellTableView.dataSource = self
        sellTableView.backgroundColor =  UIColor.white
        homeContentScrlVw.addSubview(sellTableView)
    }
    func recentTradeContent(){
        recentTableView.frame = CGRect(x: (self.view.frame.size.width * 4), y: 0, width: CGFloat(self.view.frame.size.width), height: CGFloat(self.homeContentScrlVw.frame.size.height))
        recentTableView.separatorStyle = .none
        recentTableView.register(UINib(nibName: "RecentTradeTableViewCell", bundle: nil), forCellReuseIdentifier: "RecentTradeTableViewCell")
        recentTableView.tag = 4
        recentTableView.bounces = false
        recentTableView.delegate = self
        recentTableView.dataSource = self
        recentTableView.backgroundColor =  UIColor.white
        homeContentScrlVw.addSubview(recentTableView)

        self.homeContentScrlVw.contentSize = CGSize(width: CGFloat(self.view.frame.size.width) * 6, height: (self.homeContentScrlVw.frame.size.height))
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 0 {
            return 1
        }else if tableView.tag == 4{
            return 1
        }else{
            return 7
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView.tag == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TradeTableViewCell", for: indexPath) as! TradeTableViewCell
            cell.selectionStyle = .none
            cell.amountTxtFld.delegate = self
            cell.limitPriceTxtFld.delegate = self
            return cell
        }else if tableView.tag == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "OpenOrderTableVwCell", for: indexPath) as! OpenOrderTableVwCell
            cell.selectionStyle = .none
            if indexPath.row != 0{
                cell.openFirstLbl.text = "1860.00"
                cell.openSecondLbl.text = "0.00100000"
                cell.openThirdLbl.text = "1.86"
                cell.openFourthLbl.text = "1.86"
                
                cell.openFirstLbl.textColor = UIColor.init(red: 29.0/255.0, green: 42.0/255.0, blue: 89.0/255.0, alpha: 1.0)
                cell.openSecondLbl.textColor = UIColor.init(red: 29.0/255.0, green: 42.0/255.0, blue: 89.0/255.0, alpha: 1.0)
                cell.openThirdLbl.textColor = UIColor.init(red: 29.0/255.0, green: 42.0/255.0, blue: 89.0/255.0, alpha: 1.0)
                cell.openFourthLbl.textColor = UIColor.init(red: 29.0/255.0, green: 42.0/255.0, blue: 89.0/255.0, alpha: 1.0)
            }
            return cell
        }else if tableView.tag == 2 || tableView.tag == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BuyOrderTableVwCell", for: indexPath) as! BuyOrderTableVwCell
            cell.selectionStyle = .none
            if indexPath.row != 0{
                cell.buyFirstLbl.text = "1860.00"
                cell.buySecondLbl.text = "0.00100000"
                cell.buyThirdLbl.text = "1.86"
                cell.buyFourthLbl.text = "1.86"
                
                cell.buyFirstLbl.textColor = UIColor.init(red: 29.0/255.0, green: 42.0/255.0, blue: 89.0/255.0, alpha: 1.0)
                cell.buySecondLbl.textColor = UIColor.init(red: 29.0/255.0, green: 42.0/255.0, blue: 89.0/255.0, alpha: 1.0)
                cell.buyThirdLbl.textColor = UIColor.init(red: 29.0/255.0, green: 42.0/255.0, blue: 89.0/255.0, alpha: 1.0)
                cell.buyFourthLbl.textColor = UIColor.init(red: 29.0/255.0, green: 42.0/255.0, blue: 89.0/255.0, alpha: 1.0)
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RecentTradeTableViewCell", for: indexPath) as! RecentTradeTableViewCell
            cell.selectionStyle = .none
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView.tag == 0{
            return 450
        }else{
            if indexPath.row == 0{
              return 45
            }else{
              return 30
            }
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
     @objc func tradeBtnClicked(_ sender: UIButton) {
        let frame :CGRect = CGRect(x: CGFloat(self.view.frame.width) * 0, y: 0, width: CGFloat(self.view.frame.width), height: homeContentScrlVw.frame.size.height)
        self.homeContentScrlVw.scrollRectToVisible(frame, animated: true)
        
        highlightLbl.frame = CGRect(x: sender.center.x - 10, y: sender.frame.origin.y + sender.frame.size.height, width: 20, height: 4)
    }
    
   @objc func openOrdersClicked(_ sender: UIButton) {
        let frame :CGRect = CGRect(x: CGFloat(self.view.frame.width) * 1, y: 0, width: CGFloat(self.view.frame.width), height: homeContentScrlVw.frame.size.height)
        self.homeContentScrlVw.scrollRectToVisible(frame, animated: true)
    
        highlightLbl.frame = CGRect(x: sender.center.x - 10, y: sender.frame.origin.y + sender.frame.size.height, width: 20, height: 4)
    }
    
    @objc func buyOrderClicked(_ sender: UIButton) {
        let frame :CGRect = CGRect(x: CGFloat(self.view.frame.width) * 2, y: 0, width: CGFloat(self.view.frame.width), height: homeContentScrlVw.frame.size.height)
        self.homeContentScrlVw.scrollRectToVisible(frame, animated: true)
        
        highlightLbl.frame = CGRect(x: sender.center.x - 10, y: sender.frame.origin.y + sender.frame.size.height, width: 20, height: 4)
    }
    
    @objc func sellOrdersClicked(_ sender: UIButton) {
        let frame :CGRect = CGRect(x: CGFloat(self.view.frame.width) * 3, y: 0, width: CGFloat(self.view.frame.width), height: homeContentScrlVw.frame.size.height)
        self.homeContentScrlVw.scrollRectToVisible(frame, animated: true)
        
        highlightLbl.frame = CGRect(x: sender.center.x - 10, y: sender.frame.origin.y + sender.frame.size.height, width: 20, height: 4)
    }
    
    @objc func recentTradeClicked(_ sender: UIButton) {
        let frame :CGRect = CGRect(x: CGFloat(self.view.frame.width) * 4, y: 0, width: CGFloat(self.view.frame.width), height: homeContentScrlVw.frame.size.height)
        self.homeContentScrlVw.scrollRectToVisible(frame, animated: true)
        
        highlightLbl.frame = CGRect(x: sender.center.x - 10, y: sender.frame.origin.y + sender.frame.size.height, width: 20, height: 4)
    }
    
    @objc func pendingOrdersClicked(_ sender: UIButton) {
        let frame :CGRect = CGRect(x: CGFloat(self.view.frame.width) * 5, y: 0, width: CGFloat(self.view.frame.width), height: homeContentScrlVw.frame.size.height)
        self.homeContentScrlVw.scrollRectToVisible(frame, animated: true)
        
        highlightLbl.frame = CGRect(x: sender.center.x - 10, y: sender.frame.origin.y + sender.frame.size.height, width: 20, height: 4)
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if homeContentScrlVw.contentOffset.x == 0{
            highlightLbl.frame = CGRect(x: tradeBtn.center.x - 10, y: tradeBtn.frame.origin.y + tradeBtn.frame.size.height, width: 20, height: 4)
        }else if homeContentScrlVw.contentOffset.x == (CGFloat(self.view.frame.width) * 1){
            highlightLbl.frame = CGRect(x: openOrdersBtn.center.x - 10, y: openOrdersBtn.frame.origin.y + openOrdersBtn.frame.size.height, width: 20, height: 4)
        }else if homeContentScrlVw.contentOffset.x == (CGFloat(self.view.frame.width) * 2){
            highlightLbl.frame = CGRect(x: buyOrderBtn.center.x - 10, y: buyOrderBtn.frame.origin.y + buyOrderBtn.frame.size.height, width: 20, height: 4)
        }else if homeContentScrlVw.contentOffset.x == (CGFloat(self.view.frame.width) * 3){
            highlightLbl.frame = CGRect(x: sellOrdersBtn.center.x - 10, y: sellOrdersBtn.frame.origin.y + sellOrdersBtn.frame.size.height, width: 20, height: 4)
        }else if homeContentScrlVw.contentOffset.x == (CGFloat(self.view.frame.width) * 4){
            highlightLbl.frame = CGRect(x: recentTradeBtn.center.x - 10, y: recentTradeBtn.frame.origin.y + recentTradeBtn.frame.size.height, width: 20, height: 4)
        }else{
            highlightLbl.frame = CGRect(x: pendingOrdersBtn.center.x - 10, y: pendingOrdersBtn.frame.origin.y + pendingOrdersBtn.frame.size.height, width: 20, height: 4)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
