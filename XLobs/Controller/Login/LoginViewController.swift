//
//  LoginViewController.swift
//  XLobs
//
//  Created by apple on 9/18/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import FirebaseCore
import Firebase
import UserNotifications
import Alamofire
import SwiftyJSON

class LoginViewController: UIViewController,UITextFieldDelegate,SWRevealViewControllerDelegate {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var LogtitleButton: UIButton!
    @IBOutlet weak var regtitleBtn: UIButton!
    //login
    @IBOutlet weak var LoginView: UIView!
    @IBOutlet weak var loginPhNoTxtFld: FloatLabelTextField!
    @IBOutlet weak var passwordTxtfld: FloatLabelTextField!
    @IBOutlet weak var loginButton: UIButton!
    //register
    @IBOutlet weak var registerView: UIView!
    @IBOutlet weak var phoneNoTxtfld: FloatLabelTextField!
    @IBOutlet weak var regPassTxtfld: FloatLabelTextField!
    @IBOutlet weak var checkBoxBtn: UIButton!
    @IBOutlet weak var termPrivacyLbl: UILabel!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var reEnterPassTxtFld: FloatLabelTextField!
    
    @IBOutlet weak var countryCodeBtn: UIButton!
    @IBOutlet weak var countryTitleLbl: UILabel!
    let blurView = UIView()
    let popUpView = UIView()
    var countryCodeVw = UIView()
    var enterOTPTxtFld = FloatLabelTextField()
    var logCodeSelected = Bool()
    @IBOutlet weak var logCountryLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        revealViewController().delegate = self
        LoginView.isHidden = false
        registerView.isHidden = true
        headerView.layer.shadowColor = UIColor(red: 22.0/255.0, green: 17.0/255.0, blue: 44.0/255.0, alpha: 0.05).cgColor
        headerView.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        headerView.layer.shadowOpacity = 10.0
        headerView.layer.shadowRadius = 3
        headerView.layer.masksToBounds = false
        
        loginButton.layer.cornerRadius = 5
        loginButton.clipsToBounds = true
        registerButton.layer.cornerRadius = 5
        registerButton.clipsToBounds = true
    
//        let myMutableString = NSMutableAttributedString(string: "I agree to Xlobs Terms & Conditions and Privacy policy", attributes: [NSFontAttributeName:UIFont(name: "Lato-Regular", size: 13.0)!])
//
//        myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor(red: 62.0/255.0, green:  114.0/255.0, blue: 253.0/255.0, alpha: 1.0), range: NSRange(location:17,length:18))
//
//        termPrivacyLbl.attributedText = myMutableString
//        termPrivacyLbl.font = UIFont(name: "Lato-Regular", size: 17)
//        termPrivacyLbl.numberOfLines = 0
//        termPrivacyLbl.lineBreakMode = .byWordWrapping
//        termPrivacyLbl.contentMode = .scaleToFill
//        let text = (termPrivacyLbl.text)!
//        let range1 = (text as NSString).range(of:"Terms and Agreement")
//        print(range1)
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapLabel))
//        termPrivacyLbl.addGestureRecognizer(tapGesture)
//        termPrivacyLbl.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped(gesture:)))
        self.view.addGestureRecognizer(tapGesture)
        
        blurView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        blurView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        self.view.addSubview(blurView)
        blurView.isHidden = true
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(doneButtonAction))
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        phoneNoTxtfld.keyboardType = .numberPad
        phoneNoTxtfld.inputAccessoryView = doneToolbar
        enterOTPTxtFld.inputAccessoryView = doneToolbar
    }
    //MARK:- DONE BUTTO CLICKED
    @objc func doneButtonAction() {
        enterOTPTxtFld.resignFirstResponder()
        phoneNoTxtfld.resignFirstResponder()
    }
    @objc func viewTapped(gesture:UITapGestureRecognizer){
       self.view.endEditing(true)
    }
    //TapGesture in terms and Agreement
    //MARK:- TAPGESTURE
//    @objc func tapLabel(gesture: UITapGestureRecognizer) {
//        let text = (termPrivacyLbl.text)!
//        let termsRange = (text as NSString).range(of:"Terms & Agreement")
//        print(termsRange.toRange()!)
//        if gesture.didTapAttributedTextInLabel(label: termPrivacyLbl, inRange: termsRange) {
////            let alert = AlertView.alertWithTitle(title: "", message: "")
////            alert.addButtons(buttonArray: ["Ok"])
//            print("Tapped terms")
//        } else {
//            print("Tapped none")
//        }
//    }
    @IBAction func regtitleClicked(_ sender: Any) {
        LoginView.isHidden = true
        registerView.isHidden = false
        regtitleBtn.setTitleColor(UIColor(red: 42.0/255.0, green: 55.0/255.0, blue: 99.0/255.0, alpha: 1.0), for: .normal)
        LogtitleButton.setTitleColor(UIColor(red: 202.0/255.0, green:  213.0/255.0, blue: 223.0/255.0, alpha: 1.0), for: .normal)
        countryCodeVw.isHidden = true
        phoneNoTxtfld.text = ""
        regPassTxtfld.text = ""
        reEnterPassTxtFld.text = ""
    }
    
    @IBAction func LogBtnClicked(_ sender: Any) {
        LoginView.isHidden = false
        registerView.isHidden = true
        LogtitleButton.setTitleColor(UIColor(red: 42.0/255.0, green: 55.0/255.0, blue: 99.0/255.0, alpha: 1.0), for: .normal)
        regtitleBtn.setTitleColor(UIColor(red: 202.0/255.0, green:  213.0/255.0, blue: 223.0/255.0, alpha: 1.0), for: .normal)
        countryCodeVw.isHidden = true
        loginPhNoTxtFld.text = ""
        passwordTxtfld.text = ""
    }
    
    @IBAction func loggedInClicked(_ sender: Any) {
        if loginPhNoTxtFld.text! == "" && passwordTxtfld.text! == ""{
            AlertHelper.alertBox(Mymsg: "please enter all fields", view: self)
        }else if loginPhNoTxtFld.text! == ""{
            AlertHelper.alertBox(Mymsg: "please enter phone number", view: self)
        }else if passwordTxtfld.text! == ""{
            AlertHelper.alertBox(Mymsg: "please enter Password", view: self)
        }else{
            if NetworkingManager.isConnectedToNetwork(){
                showActivityIndicator()
                let signInParams = ["user_phone": loginPhNoTxtFld.text!,"user_password": passwordTxtfld.text!,"ip": "106.51.64.15"]
                print(signInParams)
                let userHeaders: HTTPHeaders = ["Content-Type": "application/json"]
                Alamofire.request(logInURL, method: HTTPMethod.post, parameters: signInParams,encoding: JSONEncoding.default, headers: userHeaders).responseJSON { response in
                    switch(response.result) {
                    case .success(let value):
                        let json = JSON(value)
                        print(json)
                        let dataDict = json["response"].dictionary!
                        if response.response?.statusCode == 200{
                            if dataDict["success"]?.bool! == false{
                                AlertHelper.alertBox(Mymsg: "Already registered with this email", view: self)
                            }else{
                                let customerId = (dataDict["data"]?.dictionary!["customer_id"]?.string!)
                                UserDefaults.standard.setValue(customerId!, forKey: "customer_id")
                                UserDefaults.standard.synchronize()
                                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SWRevealViewController") as? SWRevealViewController
                                self.navigationController?.pushViewController(vc!, animated: true)
                            }
                        }else if response.response?.statusCode == 401{
                            AlertHelper.alertBox(Mymsg: "user not found!", view: self)
                        }

                    case .failure(_):
                        print(response.result.error!)
                        break
                    }
                    debugPrint(response)
                    hideActivityIndicator()
                }
            }else{
                NetworkingManager.netWorkFailed(view: self)
            }
        }
    }
    
    @IBAction func checkBoxClicked(_ sender: Any) {
        if checkBoxBtn.currentImage == UIImage(named: "uncheckbox"){
            checkBoxBtn.setImage(UIImage(named: "checkbox"), for: .normal)
        }else{
            checkBoxBtn.setImage(UIImage(named: "uncheckbox"), for: .normal)
        }
    }
    @IBAction func registerationClicked(_ sender: Any) {
        if phoneNoTxtfld.text! == "" && regPassTxtfld.text! == "" && reEnterPassTxtFld.text! == ""{
            AlertHelper.alertBox(Mymsg: "please enter all fields", view: self)
        }
        else if phoneNoTxtfld.text! == ""{
            AlertHelper.alertBox(Mymsg: "please enter Phone number", view: self)
        }else if regPassTxtfld.text! == ""{
            AlertHelper.alertBox(Mymsg: "please enter Password", view: self)
        }else if reEnterPassTxtFld.text! == ""{
            AlertHelper.alertBox(Mymsg: "please re-enter Password", view: self)
        }else if (regPassTxtfld.text! != reEnterPassTxtFld.text!){
            AlertHelper.alertBox(Mymsg: "Both passwords doesn't match", view: self)
        }
        else{
//            verifyClicked()
            blurView.isHidden = false
            popUpView.isHidden = false

            popUpView.frame = CGRect(x: 0, y: 0, width: 300, height: 200)
            popUpView.center = self.blurView.center
            popUpView.backgroundColor = UIColor.white
            popUpView.layer.cornerRadius = 10
            popUpView.clipsToBounds = true
            self.view.addSubview(popUpView)

            let cancelBtn = UIButton.init(frame: CGRect(x: popUpView.frame.size.width-40, y: 10, width: 30, height: 30))
            cancelBtn.setImage(UIImage(named:"cross_blue"), for: .normal)
            cancelBtn.addTarget(self, action: #selector(cancelClicked), for: .touchUpInside)
            popUpView.addSubview(cancelBtn)

            enterOTPTxtFld.frame = CGRect(x: 50, y: 40, width: 200, height: 40)
            enterOTPTxtFld.placeholder = "Enter OTP"
            enterOTPTxtFld.font = UIFont(name: "Nunito-Regular", size: 16)
            enterOTPTxtFld.autocorrectionType = .no
            enterOTPTxtFld.keyboardType = .numberPad
            enterOTPTxtFld.delegate = self
            popUpView.addSubview(enterOTPTxtFld)

            verifyWithFirebase(phonenumber: phoneNoTxtfld.text!)

            let verifyOtpBtn = UIButton.init(frame: CGRect(x: 50, y: popUpView.frame.size.height - 100, width: 200, height: 40))
            verifyOtpBtn.setTitle("Verify OTP to SignUp", for: .normal)
            verifyOtpBtn.titleLabel?.font = UIFont(name: "Nunito-Regular", size: 14)
            verifyOtpBtn.layer.cornerRadius = verifyOtpBtn.frame.size.height/2
            verifyOtpBtn.backgroundColor = UIColor(red: 250.0/255.0, green: 200.0/255.0, blue: 0.0/255.0, alpha: 1.0)
            verifyOtpBtn.addTarget(self, action: #selector(verifyClicked), for: .touchUpInside)
            popUpView.addSubview(verifyOtpBtn)

            let resendOTP = UIButton.init(frame: CGRect(x: 0, y: popUpView.frame.size.height - 30, width: popUpView.frame.size.width, height: 20))
            resendOTP.setTitle("Resend OTP", for: .normal)
            resendOTP.titleLabel?.font = UIFont(name: "Nunito-Regular", size: 14)
            resendOTP.setTitleColor(UIColor(red: 250.0/255.0, green: 200.0/255.0, blue: 0.0/255.0, alpha: 1.0), for: .normal)
            resendOTP.addTarget(self, action: #selector(resendClicked), for: .touchUpInside)
            popUpView.addSubview(resendOTP)
        }
    }
    //MARK:- VERIFY EMAIL
    func isValidEmailAddress(emailAddressString: String) -> Bool {
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            if results.count == 0{
                returnValue = false
            }
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        return  returnValue
    }
    //MARK:- CANCEL CLICKED
    @objc func cancelClicked(){
        blurView.isHidden = true
        popUpView.isHidden = true
    }
    //MARK:- VERIFY CLICKED
    @objc func verifyClicked(){
        if NetworkingManager.isConnectedToNetwork(){
            if enterOTPTxtFld.text! == ""{
                AlertHelper.alertBox(Mymsg: "Enter OTP", view: self)
            }else{
                let credentials : PhoneAuthCredential = PhoneAuthProvider.provider().credential(withVerificationID: UserDefaults.standard.value(forKey: "authVerificationID") as! String, verificationCode: enterOTPTxtFld.text!)
                Auth.auth().signInAndRetrieveData(with: credentials) { (user, error) in
                    if (error != nil){
                        print(error.debugDescription)
                    }else{
                        showActivityIndicator()
//                        var userCountry = String()
//                        if self.countryTitleLbl.text! == "+91"{
//                            userCountry = "India"
//                        }else{
//                            userCountry = "UnitedStates"
//                        }
                print(self.countryTitleLbl.text!)
                        let signUpParams = ["user_password": self.regPassTxtfld.text!,"user_phone": self.phoneNoTxtfld.text!, "user_phone_country": self.countryTitleLbl.text!, "ip": "106.51.64.15"]
                        
                        let userHeaders: HTTPHeaders = ["Content-Type": "application/json"]
                        Alamofire.request(signUpURL, method: HTTPMethod.post, parameters: signUpParams,encoding: JSONEncoding.default, headers: userHeaders).responseJSON { response in
                            switch(response.result) {
                            case .success(let value):
                                let json = JSON(value)
                                print(json)
                                print(json["response"].dictionary!)
                                let dataDict = json["response"].dictionary!
                                if response.response?.statusCode == 200{
                                    if dataDict["success"]?.bool! == false{
                                        AlertHelper.alertBox(Mymsg: "Already registered with this email", view: self)
                                    }else{
                                        let customerId = (dataDict["data"]?.dictionary!["customer_id"]?.string!)
                                        UserDefaults.standard.setValue(customerId!, forKey: "customer_id")
                                        UserDefaults.standard.synchronize()
                                        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SWRevealViewController") as? SWRevealViewController
                                        self.navigationController?.pushViewController(vc!, animated: true)
                                    }
                                }else if response.response?.statusCode == 401 {
                                    AlertHelper.alertBox(Mymsg: "Already registered with this email", view: self)
                                    self.popUpView.isHidden = true
                                }
                            case .failure(_):
                                print(response.result.error!)
                                break
                            }
                            debugPrint(response)
                            hideActivityIndicator()
                        }
                    }
                }
            }
        }else{
            NetworkingManager.netWorkFailed(view: self)
        }
        
    }
    //MARK:-TETXFIELD DELEGATES
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == regPassTxtfld{
            self.view.frame.origin.y -= 60
        }else if textField == reEnterPassTxtFld{
            self.view.frame.origin.y -= 80
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == regPassTxtfld{
            self.view.frame.origin.y += 60
        }else if textField == reEnterPassTxtFld{
            self.view.frame.origin.y += 80
        }
    }
    //MARK:- RESEND OTP CLICKED
    @objc func resendClicked(){
        verifyWithFirebase(phonenumber: phoneNoTxtfld.text!)
    }
    func verifyWithFirebase(phonenumber : String){
        print(countryTitleLbl.text!)
         print(phoneNoTxtfld.text!)
        PhoneAuthProvider.provider().verifyPhoneNumber("\(countryTitleLbl.text!)\(phoneNoTxtfld.text ?? "")", uiDelegate: nil) { (verificationID, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func logCountryClicked(_ sender: Any) {
        countrySelection()
        logCodeSelected = true
    }
    @IBAction func countryCodeClicked(_ sender: Any) {
        countrySelection()
        logCodeSelected = false
    }
    func countrySelection(){
        countryCodeVw.isHidden = false
        print(countryCodeBtn.frame.origin.y)
        countryCodeVw.frame = CGRect(x: countryCodeBtn.frame.origin.x, y:registerView.frame.origin.y + countryCodeBtn.frame.origin.y + countryCodeBtn.frame.size.height, width: countryCodeBtn.frame.size.width, height: (countryCodeBtn.frame.size.height * 2)+1)
        countryCodeVw.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        self.view.addSubview(countryCodeVw)
        
        let indianCodeBtn = UIButton.init(frame: CGRect(x: 0, y: 0, width: countryCodeVw.frame.size.width, height: countryCodeBtn.frame.size.height))
        indianCodeBtn.setTitle("+91", for: .normal)
        indianCodeBtn.setTitleColor(UIColor.white, for: .normal)
        indianCodeBtn.titleLabel?.font = UIFont(name: "Lato-Regular", size: 14)
        indianCodeBtn.backgroundColor = UIColor.clear
        indianCodeBtn.addTarget(self, action: #selector(indianCodeClicked), for: .touchUpInside)
        countryCodeVw.addSubview(indianCodeBtn)
        
        let lineLbl = UILabel.init(frame: CGRect(x: 0, y: indianCodeBtn.frame.origin.y + indianCodeBtn.frame.size.height, width: countryCodeBtn.frame.size.width, height: 1))
        lineLbl.backgroundColor = UIColor.white
        countryCodeVw.addSubview(lineLbl)
        
        let USCodeBtn = UIButton.init(frame: CGRect(x: 0, y: indianCodeBtn.frame.origin.y + indianCodeBtn.frame.size.height + 1, width: countryCodeBtn.frame.size.width, height: countryCodeBtn.frame.size.height))
        USCodeBtn.setTitle("+1", for: .normal)
        USCodeBtn.backgroundColor = UIColor.clear
        USCodeBtn.setTitleColor(UIColor.white, for: .normal)
        USCodeBtn.titleLabel?.font = UIFont(name: "Lato-Regular", size: 14)
        USCodeBtn.addTarget(self, action: #selector(USCodeClicked), for: .touchUpInside)
        countryCodeVw.addSubview(USCodeBtn)
    }
    //MARK:- INDIAN CLICKED
    @objc func indianCodeClicked(){
        if logCodeSelected == true{
            logCountryLbl.text = "+91"
        }else{
            countryTitleLbl.text = "+91"
        }
        countryCodeVw.isHidden = true
    }
    //MARK:- US CODE CLICKED
    @objc func USCodeClicked(){
        if logCodeSelected == true{
            logCountryLbl.text = "+1"
        }else{
            countryTitleLbl.text = "+1"
        }
        countryCodeVw.isHidden = true
    }
    @IBAction func backClicked(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
}
