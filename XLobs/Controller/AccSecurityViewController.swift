//
//  AccSecurityViewController.swift
//  XLobs
//
//  Created by apple on 9/19/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit

class AccSecurityViewController: UIViewController {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var securityOptionsVw: UIView!
    
    @IBOutlet weak var menuButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        headerView.layer.shadowColor = UIColor(red: 22.0/255.0, green: 17.0/255.0, blue: 44.0/255.0, alpha: 0.05).cgColor
        headerView.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        headerView.layer.shadowOpacity = 10.0
        headerView.layer.shadowRadius = 3
        headerView.layer.masksToBounds = false
        
        
        securityOptionsVw.layer.shadowColor = UIColor(red: 22.0/255.0, green: 17.0/255.0, blue: 44.0/255.0, alpha: 0.05).cgColor
        securityOptionsVw.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        securityOptionsVw.layer.shadowOpacity = 25.0
        securityOptionsVw.layer.shadowRadius = 10.0
        securityOptionsVw.layer.masksToBounds = false
        
        menuButton.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.navigationController?.navigationBar.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func menuClicked(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }

}
