//
//  ProfileViewController.swift
//  XLobs
//
//  Created by apple on 9/19/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ProfileViewController: UIViewController,UIScrollViewDelegate {

    @IBOutlet weak var profileScrollVw: UIScrollView!
    @IBOutlet weak var profileContentVw: UIView!
    @IBOutlet weak var fullNameTxtfld: FloatLabelTextField!
    @IBOutlet weak var emailTxtfld: FloatLabelTextField!
    @IBOutlet weak var contactNoTxtFld: FloatLabelTextField!
    
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var autoLogBtn: UIButton!
    
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var accountIdLbl: UILabel!
    @IBOutlet weak var defaultButton: UIButton!
    
    var profileArray = Array<Any>()

    override func viewDidLoad() {
        super.viewDidLoad()
        profileScrollVw.bounces = false
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped(gesture:)))
        self.view.addGestureRecognizer(tapGesture)
        
        menuButton.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        self.navigationController?.navigationBar.isHidden = true
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }
    @objc func viewTapped(gesture:UITapGestureRecognizer){
        self.view.endEditing(true)
    }
    //MARK:- VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
//        fetchUserProfileDetails()
        self.navigationController?.isNavigationBarHidden = true
    }
    
    //MARK:- FETCH USER PROFILE DETAILS
    func fetchUserProfileDetails(){
        if NetworkingManager.isConnectedToNetwork(){
            showActivityIndicator()
            let Headers: HTTPHeaders = ["Content-Type": "application/json"]
            let customerId = UserDefaults.standard.value(forKey: "customer_id")
            let profileParams = ["query":["customer_id":customerId],"show":[],"sort":["createdAt":"-1"]] as [String : Any]
            Alamofire.request(GetUserProfileURL, method: .post, parameters: profileParams, encoding: JSONEncoding.default, headers: Headers).responseJSON { response in
                switch (response.result){
                case .success (let value):
                    hideActivityIndicator()
                    let json = JSON(value)
                    let dataDict = json["response"].dictionary!
                    if response.response?.statusCode == 200{
                        if dataDict["success"]?.bool! == false{
                            AlertHelper.alertBox(Mymsg: "user details not found!", view: self)
                        }else{
                            let profileArray = (dataDict["data"]?.array)!
                            for value in profileArray{
                                self.userNameLbl.text = value["user_full_name"].string!
                                self.accountIdLbl.text = value["user_account_id"].string!
                                self.fullNameTxtfld.text = value["user_full_name"].string!
                                self.emailTxtfld.text = value["user_email"].string!
                                self.contactNoTxtFld.text = value["user_phone"].string!
                                self.autoLogBtn.setTitle("Default (\(value["user_auto_logout_time"].int!) minutes)", for: .normal)
                                self.defaultButton.setTitle("Default (\(value["user_auto_logout_time"].int!) minutes)", for: .normal)
                            }
                        }
                    }else if response.response?.statusCode == 401{
                        AlertHelper.alertBox(Mymsg: "user details not found!", view: self)
                    }
                case .failure:
                    hideActivityIndicator()
                    break
                }
                debugPrint(response)
            }
        }else{
            NetworkingManager.netWorkFailed(view: self)
        }
    }
    
    override func viewWillLayoutSubviews(){
        super.viewWillLayoutSubviews()
        profileScrollVw.contentSize = CGSize(width: self.view.frame.size.width, height: 1200)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func menuClicked(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }

    @IBAction func tradingClicked(_ sender: Any) {
        let revealviewcontroller:SWRevealViewController = self.revealViewController()
        let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewcontroller = mainstoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let newFrontController = UINavigationController.init(rootViewController: newViewcontroller)
        revealviewcontroller.pushFrontViewController(newFrontController, animated: true)
    }
    @IBAction func marketsClicked(_ sender: Any) {
//        let revealviewcontroller:SWRevealViewController = self.revealViewController()
//        let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let newViewcontroller = mainstoryboard.instantiateViewController(withIdentifier: "WalletViewController") as! WalletViewController
//        let newFrontController = UINavigationController.init(rootViewController: newViewcontroller)
//        revealviewcontroller.pushFrontViewController(newFrontController, animated: true)
    }
    @IBAction func walletClicked(_ sender: Any) {
        let revealviewcontroller:SWRevealViewController = self.revealViewController()
        let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewcontroller = mainstoryboard.instantiateViewController(withIdentifier: "WalletViewController") as! WalletViewController
        let newFrontController = UINavigationController.init(rootViewController: newViewcontroller)
        revealviewcontroller.pushFrontViewController(newFrontController, animated: true)
    }
    
}
