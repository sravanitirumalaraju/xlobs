//
//  menuViewController.swift
//  XLobs
//
//  Created by apple on 9/24/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit

class menuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var menuTableVw: UITableView!
    
    var menuTitles :Array = [String]()
    var menuImages:Array = [UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        menuTitles = ["Trading","Wallet","Profile","Security","Get Verified","About Us","Legal & Security","Terms and Conditions","Privacy Policy","Contact Us","Log Out"]
        menuTitles = ["Profile","Security","Get Verified","About Us","Legal & Security","Terms and Conditions","Privacy Policy","Contact Us","Log Out"]
        menuImages = [#imageLiteral(resourceName: "profile"),#imageLiteral(resourceName: "security"),#imageLiteral(resourceName: "verify")]
//        menuImages = [#imageLiteral(resourceName: "trading"),#imageLiteral(resourceName: "wallet"),#imageLiteral(resourceName: "profile"),#imageLiteral(resourceName: "security"),#imageLiteral(resourceName: "verify")]
        menuTableVw.delegate = self
        menuTableVw.dataSource = self
        menuTableVw.register(UINib(nibName: "menuTableViewCell", bundle: nil), forCellReuseIdentifier: "menuTableViewCell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 9
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "menuTableViewCell", for: indexPath) as! menuTableViewCell
        cell.selectionStyle = .none
//        if indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2 {
//            cell.topView.isHidden = false
//            cell.secVerTextVw.isHidden = true
//            cell.singleTextVw.isHidden = true
//            cell.menuTitle.text = menuTitles[indexPath.row]
//            cell.menuImage.image = menuImages[indexPath.row]
//            cell.topLineLbl.isHidden = true
//            if indexPath.row == 0{
//                cell.menuTitle.textColor = UIColor(red: 28.0/255.0, green: 28.0/255.0, blue: 31.0/255.0, alpha: 1.0)
//            }else{
//                cell.menuTitle.textColor = UIColor(red: 124.0/255.0, green: 123.0/255.0, blue: 160.0/255.0, alpha: 1.0)
//            }
//            if indexPath.row == 2{
//                cell.topLineLbl.isHidden = false
//            }
//        }else
        if indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2 {
            cell.topView.isHidden = true
            cell.secVerTextVw.isHidden = false
            cell.singleTextVw.isHidden = true
            cell.secVerMenuTitle.text = menuTitles[indexPath.row]
            cell.secVerImage.image = menuImages[indexPath.row]
            cell.secVerLineLbl.isHidden = true
            if indexPath.row == 2{
                cell.secVerLineLbl.isHidden = false
            }
        }else {
            cell.topView.isHidden = true
            cell.secVerTextVw.isHidden = true
            cell.singleTextVw.isHidden = false
            cell.singleTitle.text = menuTitles[indexPath.row]
            cell.singleLineLbl.isHidden = true
            if indexPath.row == 7{
                cell.singleLineLbl.isHidden = false
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 8{
            return 80
        }else{
            return 60
        }
//        if indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2{
//            return 60
//        }else if indexPath.row == 3 || indexPath.row == 4{
//            return 60
//        }else if indexPath.row == 10{
//            return 80
//        }else{
//            return 60
//        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let revealviewcontroller:SWRevealViewController = self.revealViewController()
//        if indexPath.row == 0 {
//            let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//            let newViewcontroller = mainstoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
//            let newFrontController = UINavigationController.init(rootViewController: newViewcontroller)
//            revealviewcontroller.pushFrontViewController(newFrontController, animated: true)
//        }else if indexPath.row == 1{
//            let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//            let newViewcontroller = mainstoryboard.instantiateViewController(withIdentifier: "WalletViewController") as! WalletViewController
//            let newFrontController = UINavigationController.init(rootViewController: newViewcontroller)
//            revealviewcontroller.pushFrontViewController(newFrontController, animated: true)
//        }else
        if indexPath.row == 0 {
            let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewcontroller = mainstoryboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            let newFrontController = UINavigationController.init(rootViewController: newViewcontroller)
            revealviewcontroller.pushFrontViewController(newFrontController, animated: true)
        }else if indexPath.row == 1 {
            let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewcontroller = mainstoryboard.instantiateViewController(withIdentifier: "AccSecurityViewController") as! AccSecurityViewController
            let newFrontController = UINavigationController.init(rootViewController: newViewcontroller)
            revealviewcontroller.pushFrontViewController(newFrontController, animated: true)
        }else if indexPath.row == 2 {
            let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewcontroller = mainstoryboard.instantiateViewController(withIdentifier: "KnowYCViewController") as! KnowYCViewController
            let newFrontController = UINavigationController.init(rootViewController: newViewcontroller)
            revealviewcontroller.pushFrontViewController(newFrontController, animated: true)
        }else if indexPath.row == 8{
            let destVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            UserDefaults.standard.set("", forKey: "customer_id")
            UserDefaults.standard.synchronize()
            self.navigationController?.pushViewController(destVc, animated: true)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

}
