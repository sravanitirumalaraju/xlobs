//
//  WalletViewController.swift
//  XLobs
//
//  Created by apple on 9/19/18.
//  Copyright © 2018 apple. All rights reserved.
//

import UIKit

class WalletViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var walletTableVw: UITableView!
    @IBOutlet weak var menuButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        headerView.layer.shadowColor = UIColor(red: 22.0/255.0, green: 17.0/255.0, blue: 44.0/255.0, alpha: 0.05).cgColor
        headerView.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        headerView.layer.shadowOpacity = 10.0
        headerView.layer.shadowRadius = 3
        headerView.layer.masksToBounds = false
        
        walletTableVw.register(UINib(nibName: "WalletTableViewCell", bundle: nil), forCellReuseIdentifier: "WalletTableViewCell")
        
        menuButton.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.navigationController?.navigationBar.isHidden = true
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let WalletTableViewCell = tableView.dequeueReusableCell(withIdentifier: "WalletTableViewCell", for: indexPath) as! WalletTableViewCell
        WalletTableViewCell.selectionStyle = .none
        if indexPath.row == 0{
            WalletTableViewCell.coinImageVw.image = UIImage(named: "bitcoin")
            WalletTableViewCell.coinNameLbl.text = "Bitcoin"
        }else if indexPath.row == 1{
            WalletTableViewCell.coinImageVw.image = UIImage(named: "bitcoinCash")
            WalletTableViewCell.coinNameLbl.text = "Bitcoin Cash"
        }else if indexPath.row == 2{
            WalletTableViewCell.coinImageVw.image = UIImage(named: "litecoin")
            WalletTableViewCell.coinNameLbl.text = "Litecoin"
        }else if indexPath.row == 3{
            WalletTableViewCell.coinImageVw.image = UIImage(named: "ripple")
            WalletTableViewCell.coinNameLbl.text = "Ripple"
        }else if indexPath.row == 4{
            WalletTableViewCell.coinImageVw.image = UIImage(named: "ethereum")
            WalletTableViewCell.coinNameLbl.text = "Ethereum"
        }else if indexPath.row == 5{
            WalletTableViewCell.coinImageVw.image = UIImage(named: "lobstex")
            WalletTableViewCell.coinNameLbl.text = "Lobstex"
        }
        return WalletTableViewCell
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func menuClicked(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }

}
